package fr.gouv.tac.analytics.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AnalyticsServerApplication {

    public static void main(final String[] args) {
        SpringApplication.run(AnalyticsServerApplication.class, args);
    }
}
