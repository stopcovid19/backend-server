package fr.gouv.tacw.model;

public class OpaqueStaticVisit extends OpaqueVisit {

	public OpaqueStaticVisit(String payload, long visitTime) {
		super(payload, visitTime);
	}

}
