package fr.gouv.tacw.ws.utils;

public interface UriConstants {

	public static final String REPORT = "/wreport";
	public static final String STATUS = "/wstatus";

	public static final String API_V1 = "/v1";
    public static final String API_V2 = "/v2";
}
