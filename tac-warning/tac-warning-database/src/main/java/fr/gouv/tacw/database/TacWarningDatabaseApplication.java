package fr.gouv.tacw.database;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TacWarningDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(TacWarningDatabaseApplication.class, args);
	}

}
