package fr.gouv.tac.systemtest;

import fr.gouv.tac.tacwarning.model.Visit;

public class MyVisit extends Visit {
    String name ;

    public MyVisit(final String name) {
        this.name = name;
    }
}
