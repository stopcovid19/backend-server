package fr.gouv.stopc.robert.pushnotif.batch.utils;

public final class PushBatchConstants {

    private PushBatchConstants() {

        throw new AssertionError();
    }

    public final static String MIN_ID = "minId";
    public final static String MAX_ID = "maxId";
    public final static String PUSH_DATE = "pushDate";
}
